package com.oneSoft.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity

public class Formation implements Serializable,Comparable<Formation>{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	
	private long id_formation;
	private String titre;
	private int nombreParticipant;
	
	@Column(length=100)
	private String dateAjout;
	
	private String theme;
	private String langue;
	private String discipline;
	private String lien_programme;
	private String lien_auxilliaire_programme;
	
	
	@ManyToOne
	@JoinColumn(name="id_proprietaire")
	private Utilisateur proprietaire;
	
	@ManyToMany(mappedBy="formationsEnregistres")
	private List<Utilisateur> enregistrerPar;

	@Override
	public String toString() {
		return "Document [id_document=" + id_formation + ", titre=" + titre + ", nombrePage=" + nombreParticipant
				+ ", dateAjout=" + dateAjout + ", theme=" + theme + ", langue=" + langue + ", discipline=" + discipline
				+ ", lien=" + lien_programme + ", lien_auxilliaire=" + lien_auxilliaire_programme+ "]";
	}
	
	
	@Override
	public int compareTo(Formation o) {
		// TODO Auto-generated method stub
		if(this.id_formation==o.getId_formation()) return 0;
		return 1;
	}
	
	
	
	
	
	
	

}