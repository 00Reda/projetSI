package com.oneSoft.web;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.oneSoft.dao.IEspacePublicRepository;

import com.oneSoft.dao.FormateurRep;
import com.oneSoft.entities.Formation;
import com.oneSoft.entities.Participant;

import com.oneSoft.entities.Formateur;

@Controller
public class EspaceAdminController {
    @Autowired
    private FormateurRep profRep;
    @Autowired
	 private IEspacePublicRepository espacePublicRep ;
    
    
		@RequestMapping(value="/dashboard/admin/prof")
			
			public String AllProf(Model model,@RequestParam(name="page",defaultValue="0")int page ) {
				Page<Formateur> Formateurs= (Page<Formateur>) profRep.findAll(PageRequest.of(page, 5));
				  Collection<Formateur> Formateur=Formateurs.getContent();
				  
				   model.addAttribute("total", Formateurs.getTotalPages() );
				   model.addAttribute("Formateur", Formateur );
				   
				   
				return "EspaceAdmin/Prof/info";
			}
		
		@RequestMapping(value="/dashboard/admin/prof/Formations")
		
		public String FormationsProf(Model model,@RequestParam(name="id")long id,@RequestParam(name="page",defaultValue="0")int page ) {
			
			 Formateur prof=profRep.getOne(id);
			 Page<Formation> Formations= (Page<Formation>) espacePublicRep.findByProprietaire(prof, PageRequest.of(page, 9));
			 Collection<Formation> Formation=Formations.getContent();
			 boolean empty=false;
			 if(Formations.isEmpty()) {
				 empty=true;
			 }
			 model.addAttribute("empty", empty);
			 model.addAttribute("prof", prof);
			 model.addAttribute("total", Formations.getTotalPages());
			 model.addAttribute("Formations", Formation);
			return "EspaceAdmin/Prof/Formationprof";
		}
		
		@RequestMapping(value="/dashboard/admin/delete/prof")
		
		public String deleteProf(Model model,@RequestParam(name="id") long id ) {
		      profRep.deleteById(id);
			  return "redirect:/dashboard/admin/prof";
		}
     @RequestMapping(value="/dashboard/admin/prof/Formation/delete")
		
		public String deleteDoc(Model model,@RequestParam(name="id")long id ,@RequestParam(name="email")String email) {
    	      espacePublicRep.deleteById(id);
			  return "redirect:/dashboard/admin/prof/Formations?id="+email;
		}
		
     @RequestMapping(value="/dashboard/admin/bookmark")
	 public String bookmark(Model model,Authentication auth) {
		 
		 return "redirect:/dashboard/Partcipant";
	 }	
     
     @RequestMapping(value="/dashboard/admin/calendar")
	 public String calendar(Model model,Authentication auth) {
		 
		 return "EspaceAdmin/calendar";
	 }	
     
    

}
