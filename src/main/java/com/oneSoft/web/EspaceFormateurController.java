package com.oneSoft.web;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.oneSoft.EDocApplication;
import com.oneSoft.SecurityConfig;
import com.oneSoft.dao.FormationRep;
import com.oneSoft.dao.FormateurRep;
import com.oneSoft.entities.Participant;
import com.oneSoft.entities.Formateur;
import com.oneSoft.entities.Formation;
import com.oneSoft.entities.Roles;

@Controller
public class EspaceFormateurController{

	@Autowired
	private FormateurRep formateurRep;
	@Autowired
	private FormationRep formationRep;
	
	@RequestMapping(value="/dashboard/prof/home")
	
	public String home(Model model ,Principal principal) {
		
		Formateur p=formateurRep.getOne(Long.parseLong(principal.getName()));
		model.addAttribute("docs",formateurRep.DocOfProf(p));
		
		return "EspaceFormateur/home";
	}
	
	@RequestMapping(value="/dashboard/prof/bookmark")
	
	public String bookmark(Model model ,Principal principal) {
		
		Formateur p=formateurRep.getOne(Long.parseLong(principal.getName()));
		model.addAttribute("docs",p.getFormationsEnregistres());
		
		return "EspaceFormateur/bookmark";
	}
	
	@RequestMapping(value="/users/prof/info")
	
	public String info(Model model ,Principal principal) {
		
		Formateur p=formateurRep.getOne(Long.parseLong(principal.getName()));
		model.addAttribute("p",p);
		
		return "EspaceFormateur/info";
	}
	
    @RequestMapping(value="/users/prof/edit")
	
	public String edit(Model model ,Principal principal) {
		
		Formateur p=formateurRep.getOne(Long.parseLong(principal.getName()));
		model.addAttribute("p",p);
		
		return "EspaceFormateur/edit";
	}
    						
    @RequestMapping(value="/users/prof/edit/action", method=RequestMethod.POST)
	
	public String editFormAction(Model model ,Principal principal,Formateur prof) {
		
    	
    	
		Formateur p=formateurRep.getOne(Long.parseLong(principal.getName()));
		prof.setRoles(p.getRoles());
		prof.setId_user(p.getId_user());
		prof.setFormationsEnregistres(p.getFormationsEnregistres());
		if(prof.getPassword().equals("")) {
			prof.setPassword(p.getPassword());
		}else {
			String s=SecurityConfig.crypter(prof.getPassword());
			prof.setPassword(s);
		}
		
		try {
			formateurRep.save(prof);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			model.addAttribute("p",p);
			model.addAttribute("error",e.getMessage());
			return "EspaceFormateur/edit";
		}
		
		return "redirect:/users/prof/info";
	}
	
	
    @RequestMapping(value="/docs/prof/delete/{id}")
	
	public String delete(Model model ,Principal principal, @PathVariable long id) {
		
    	Formateur p=formateurRep.getOne(Long.parseLong(principal.getName()));
		formationRep.delete(id, p);
		return "redirect:/dashboard/prof/home";
		
	}
    
   @RequestMapping(value="/docs/prof/form")
	public String Docform(Model model ,Principal principal, @RequestParam(name="id" , required =false) String id) {
		
    	Formateur p=formateurRep.getOne(Long.parseLong(principal.getName()));
    	Formation d;
		if(id==null) {
			d=new Formation();
			model.addAttribute("action","add");
			model.addAttribute("btn","ajouter");
		}else {
			long idDoc;
			try {
				idDoc=Long.parseLong(id);
			} catch (Exception e) {
				// TODO: handle exception
				return "redirect:/dashboard/prof/home";
			}
			
			d=formationRep.findByIdAndOwner(idDoc, p);
			if(d==null) return "redirect:/dashboard/prof/home";
			
			model.addAttribute("action","edit/"+d.getId_formation());
			model.addAttribute("btn","modifier");
		}
		
		model.addAttribute("doc",d);
		return "/EspaceFormateur/Formation/form";
		
	}
   
   @RequestMapping(value="/docs/prof/action/add")
	public String DocformAddAction(Model model ,Principal principal,Formation doc ) {
		
     	Formateur p=formateurRep.getOne(Long.parseLong(principal.getName()));
     	doc.setProprietaire(p);
		doc.setDateAjout(EDocApplication.docDataFormat.format(new Date()));
     	formationRep.save(doc);
	 
	 return "redirect:/dashboard/prof/home";
		
	}
   
   @RequestMapping(value="/docs/prof/action/edit/{id}")
	public String DocformEditAction(Model model ,Principal principal,Formation doc ,@PathVariable long id ) {
		
	    Formation d=formationRep.getOne(id);
	    
	 
	    doc.setId_formation(id);
	   
	    doc.setDateAjout(d.getDateAjout());
	    doc.setProprietaire(d.getProprietaire());
    
    	try {
    		formationRep.saveAndFlush(doc);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			
		}
	 return "redirect:/dashboard/prof/home";
		
	}
    
   @RequestMapping(value="/dashboard/prof/calendar")
	 public String calendar(Model model,Authentication auth) {
		 
		 return "EspaceFormateur/calendar";
	 }	
	
    
	
}
