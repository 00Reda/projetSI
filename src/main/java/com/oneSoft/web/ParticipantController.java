package com.oneSoft.web;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.oneSoft.SecurityConfig;
import com.oneSoft.dao.PartcipantRep;
import com.oneSoft.dao.IEspacePublicRepository;
import com.oneSoft.dao.UtilisateurRep;
import com.oneSoft.entities.Formation;
import com.oneSoft.entities.Participant;
import com.oneSoft.entities.Roles;
import com.oneSoft.entities.Utilisateur;
@Controller
public class ParticipantController {
	
	@Autowired
	 private UtilisateurRep userRep ;
	
	@Autowired
	 private PartcipantRep partcipantRep ;
	
	@Autowired
	 private IEspacePublicRepository espacePublicRep ;
	
	
	@RequestMapping(value="/dashboard/Participant/bookmark")
	 public String bookmark(Model model,Authentication auth) {
		 
		 return "redirect:/dashboard/Participant";
	 }
	
	 @RequestMapping(value="/dashboard/Participant")
	 public String Participant(Model model,Authentication auth) {
		 
		 Utilisateur e = userRep.getOne(Long.parseLong(auth.getName()));
		Collection<Formation> docenregistrer=e.getFormationsEnregistres();
		 
		 model.addAttribute("bookmark", docenregistrer);
		 return "EspaceParticipant/home";
	 }
	
	
	@RequestMapping(value="/dashboard/Participant/info", method=RequestMethod.GET)
	 public String infoParticipant(Model model,Authentication auth,boolean exits) {
		 boolean errors=exits;
		 Participant e = partcipantRep.getOne(Long.parseLong(auth.getName()));
		 model.addAttribute("Participant",e);
		
		 model.addAttribute("errors",errors);
		 return "EspaceParticipant/info";
	 }
   
   @RequestMapping(value="/dashboard/Participant/edit", method=RequestMethod.GET)
	 public String editParticipantForm(Model model,Authentication auth) {

	   Participant e = partcipantRep.getOne(Long.parseLong(auth.getName()));
       
		 model.addAttribute("Participant",e);
		 
		 return "EspaceParticipant/edit";
	 }
  
   
   @RequestMapping(value="/dashboard/Participant/action", method=RequestMethod.POST)
	 public String editParticipantAction(Model model, Participant Participant,Authentication auth) {
	   Utilisateur e = userRep.getOne(Long.parseLong(auth.getName()));
	     Participant.setFormationsEnregistres(e.getFormationsEnregistres());
	     Participant.setPassword(e.getPassword());
	     
	         Participant.setDate_creation(new Date());
	         ArrayList<Roles> role= new ArrayList<>();
             role.add(new Roles("Participant"));
             Participant.setEmail(Participant.getEmail());
             Participant.setId_user(e.getId_user());
	         Participant.setRoles(role);
	    
	    	 partcipantRep.save(Participant);
	    	 if(Participant.getEmail().equals(e.getEmail()))
			 return "redirect:/dashboard/Participant";
	    	 else 
	    		 return "redirect:/logout";	 
	     
	     
	    
	 }
   
   @RequestMapping(value="/dashboard/Participant/pass/action", method=RequestMethod.POST)
	 public String editParticipantpassAction(Model model,Authentication auth,@RequestParam String oldpass,@RequestParam String newpass,@RequestParam String confirmpass) {
	   Utilisateur e = userRep.getOne(Long.parseLong(auth.getName()));
         
	     if(!newpass.equals(confirmpass)|| !SecurityConfig.verifyPass(oldpass,e.getPassword())) {
	    	 
			  return this.infoParticipant(model, auth,true );
			  
	     }else {
	    	 e.setPassword(SecurityConfig.crypter(newpass));
	    	 userRep.save(e);
	    	 return "redirect:/logout";
	     }
	     
	     
}
   @RequestMapping(value="/dashboard/admin/Participants", method=RequestMethod.GET)
	  public String AllParticipants(Model model,@RequestParam(name="page", defaultValue="0")int page) {
	     Page<Participant> Participants = partcipantRep.findAll(PageRequest.of(page, 20));
	     List<Participant> Participant = Participants.getContent();
	     int total= Participants.getTotalPages();
		 model.addAttribute("Participants",Participant);
		 model.addAttribute("total",total);
		 
		 return "EspaceAdmin/Participant/info";
	 } 
   
   @RequestMapping(value="/dashboard/admin/delete/Participant")
   public String deleteParticipant(@RequestParam(name="id") long id) {
	    
	 userRep.deleteById(id); 
	 
 	 return "redirect:/dashboard/admin/Participants";
  }
   
   @RequestMapping(value="/dashboard/admin/info/Participant")
   public String InfoParticipant(@RequestParam(name="id") long id,Model model, @RequestParam(name="page" ,defaultValue="0")int page) {
	 Utilisateur Participant= userRep.getOne(id); 
	 int nombretotal=Participant.getFormationsEnregistres().size();
	 Collection<Formation> docenregistrer=Participant.getFormationsEnregistres();
	 model.addAttribute("Formations", docenregistrer);
	 model.addAttribute("Participant", Participant);
	 model.addAttribute("nombretotal", nombretotal);
	 
	 
 	 return "EspaceAdmin/Participant/infoOfParticipant";
  }
	    
   @RequestMapping(value="/dashboard/Participant/calendar")
	 public String calendar(Model model,Authentication auth) {
		 
		 return "EspaceParticipant/calendar";
	 }	

   
   

}
