package com.oneSoft.web;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.management.relation.Role;

import org.hibernate.internal.build.AllowSysOut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.oneSoft.EDocApplication;
import com.oneSoft.SecurityConfig;
import com.oneSoft.dao.FormationRep;
import com.oneSoft.dao.PartcipantRep;
import com.oneSoft.dao.IEspacePublicRepository;
import com.oneSoft.dao.FormateurRep;
import com.oneSoft.dao.RoleRep;
import com.oneSoft.dao.UtilisateurRep;
import com.oneSoft.entities.Participant;
import com.oneSoft.entities.Formateur;
import com.oneSoft.entities.Formation;
import com.oneSoft.entities.Roles;
import com.oneSoft.entities.Utilisateur;

@Controller
public class EspacePublicController {

	 @Autowired
	 private IEspacePublicRepository espacePublicRep ;
	 
	 @Autowired
	 private RoleRep roleRep ;
	 @Autowired
	 private PartcipantRep participantRep ;

	 @Autowired
	 private FormateurRep formateurRep;
	 
	 @Autowired
	 private UtilisateurRep userRep;
	 
	 @Autowired
	 private FormationRep docRep;
	 
	 @RequestMapping(value="/")
	 public String 	DefaultIndex(Model model)  {
		
		 return this.index(model, 1);
	 }
	 
	 @RequestMapping(value="/{page}")
	 public String 	index(Model model,@PathVariable int page)  {
		 
		 Page<Formation> pages=espacePublicRep.findAll(PageRequest.of(page-1, 15));

		 Formation d =new Formation();
		 d.setProprietaire(new Utilisateur());
		 model.addAttribute("Formation",d);
		 
		 model.addAttribute("pages",pages);
		 model.addAttribute("path","/");
		 model.addAttribute("q","");
		 model.addAttribute("query","");
		
		 return "EspacePublic/Formations";
	 }
	 
	 @RequestMapping(value="/search")
	 public String 	search(Model model,@RequestParam String q)  {
		 
		 
		 return this.search(model, q, 1);
	 }
	 
	 @RequestMapping(value="/search/{page}")
	 public String 	search(Model model,@RequestParam String q,@PathVariable int page)  {
		 
		 Page<Formation> pages=espacePublicRep.findByTheme(q,PageRequest.of(page-1, 15));
		 model.addAttribute("pages",pages);
		
		 Formation d =new Formation();
		 d.setProprietaire(new Utilisateur());
		 model.addAttribute("Formation",d);
		 model.addAttribute("path","/search/");
		 model.addAttribute("q",q);
		 model.addAttribute("query","?q="+q);
		 return "EspacePublic/Formations";
	 } 
	 
	 @RequestMapping(value="/advencedSearch" , method= {RequestMethod.POST,RequestMethod.GET})
	 
	 public String advencedSearch(Model model, Formation d) {
		 
		 System.err.println(d.toString());   
		 
		 List<Formation> list=espacePublicRep.AdvancedSearch(d.getTitre(), d.getDateAjout(), d.getProprietaire().getNom(), d.getLangue(), d.getTheme(), d.getDiscipline());
		
		 int pz=list.size();
		 if(list.size()==0) {
			 pz=1;
		 }
		 
		 PageImpl<Formation> pages =new PageImpl<>(list,PageRequest.of(0, pz) ,pz);
		 
		 Page<Formation> ps=(Page<Formation>) pages;
		 d =new Formation();
		 d.setProprietaire(new Utilisateur());
		 model.addAttribute("Formation",d);
		 model.addAttribute("q","");
		 model.addAttribute("advanced",true);
		  
		 model.addAttribute("pages",ps);
		 
		 return "EspacePublic/Formations";
	 }
	 
	 
	 @RequestMapping(value="/account/student")
     public String CreateStudantAccountForm(Model model) {
    	 
		
		 model.addAttribute("Participant",new Participant());
		 
    	 return "EspacePublic/Participant";
     }
	 
	 @RequestMapping(value="/account/student/action" , method=RequestMethod.POST)
     public String CreateStudantAccountAction(Model model, Participant e) {
    	 
		 try {
			 ArrayList<Roles> r=new ArrayList<>();
			 r.add(roleRep.getOne("PARTICIPANT"));
			 Date date=new Date();
			 e.setDate_creation(date);
			 e.setRoles(r);
			 e.setPassword(SecurityConfig.crypter(e.getPassword()));
			 participantRep.save(e);
			 
		} catch (Exception e2) {      
			// TODO: handle exception
			 
			 model.addAttribute("Participant",new Participant());
			 model.addAttribute("error",e2.getMessage());
			 return "EspacePublic/Participant";
		}
		 
		 
    	 return "redirect:/login";
     }
	 
	 
	 @RequestMapping(value="/account/prof")
     public String CreateProfAccountForm(Model model) {
    	 
		 model.addAttribute("Participant",new Formateur());
		 
    	 return "EspacePublic/prof";
     }
	 
	 @RequestMapping(value="/account/prof/action" , method=RequestMethod.POST)
     public String CreateProfAccountAction(Model model, Formateur e) {
    	 
		 try {
			 ArrayList<Roles> r=new ArrayList<>();
			 System.out.println(roleRep.getOne("FORMATEUR").getRole());
			 r.add(roleRep.getOne("FORMATEUR"));
			 e.setRoles(r);
			 Date date=new Date();
			 e.setDate_creation(date);
			 e.setPassword(SecurityConfig.crypter(e.getPassword()));
			 formateurRep.save(e);
			 
		} catch (Exception e2) {
			// TODO: handle exception
			e2.printStackTrace();
			 model.addAttribute("Participant",new Formateur());
			 model.addAttribute("error",e2.getMessage());
			 return "EspacePublic/prof";
		}
		 
		 
    	 return "redirect:/login";
     }
	 
	 @RequestMapping(value="/bookmark/add/{id}" , method=RequestMethod.GET)
     public String addBookmark(Principal principal,@PathVariable long id) {
    	 
		 
		 Utilisateur user=userRep.getOne(Long.parseLong(principal.getName()));
		 
		 Formation doc = docRep.getOne(id);
		 
		 if(doc!=null) {
			 if(!user.getFormationsEnregistres().contains(doc)) {
				 user.getFormationsEnregistres().add(doc);
				 userRep.saveAndFlush(user);
			 }
		 }
		 
    	 return "redirect:/";
     }
	 
	 @RequestMapping(value="/bookmark/delete/{id}" , method=RequestMethod.GET)
     public String deleteBookmark(Principal principal,@PathVariable long id) {
    	 
		 
		 Utilisateur user=userRep.getOne(Long.parseLong(principal.getName()));
		 
		 Formation doc = docRep.getOne(id);
		 
		 if(doc!=null) {
			 List<Formation> docs= user.getFormationsEnregistres();
			 if(docs.contains(doc)) {
				 user.getFormationsEnregistres().remove(doc);
				 userRep.saveAndFlush(user);
			 }
		 }
		 
    	 return "redirect:/dashboard/"+user.getRoles().get(0).getRole().toLowerCase()+"/bookmark";
     }
	 
	 
}
