package com.oneSoft.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.oneSoft.entities.Formateur;
import com.oneSoft.entities.Formation;

public interface IEspacePublicRepository extends JpaRepository<Formation,Long> {
    
	@Query("select doc from Formation doc order by STR_TO_DATE(doc.dateAjout, '%d-%m-%Y') desc")
	public Page<Formation> findAll(Pageable page);
	
	@Query("select doc from Formation doc where doc.theme like %:th%")
	public Page<Formation> findByTheme(@Param("th") String n, Pageable page);
	
	@Query("select doc from Formation doc where doc.titre like %:titre% and doc.dateAjout like %:date% and doc.langue like %:langue% and doc.theme like %:theme% and doc.discipline like %:discipline% and doc.proprietaire in ( select p.email from Utilisateur p where p.nom like %:nom% or p.prenom like %:nom% )")
	public List<Formation> AdvancedSearch(@Param("titre") String titre ,@Param("date") String date, @Param("nom") String auteur ,@Param("langue") String langue , @Param("theme") String theme , @Param("discipline") String discipline);
	
	@Query("select doc from Formation doc where doc.proprietaire= :p order by STR_TO_DATE(doc.dateAjout, '%d-%m-%Y') desc")
	public Page<Formation> findByProprietaire(@Param("p") Formateur p,Pageable page);
	
	
}
