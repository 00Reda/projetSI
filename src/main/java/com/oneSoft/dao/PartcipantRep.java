package com.oneSoft.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.oneSoft.entities.Participant;

public interface PartcipantRep extends JpaRepository<Participant, Long>{
    
	
	@Query("select participant from Participant participant ")
	public Page<Participant> findAll(Pageable page);
}
