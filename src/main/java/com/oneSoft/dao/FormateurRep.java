package com.oneSoft.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.oneSoft.entities.Formateur;
import com.oneSoft.entities.Formation;

public interface FormateurRep extends JpaRepository<Formateur, Long>{

	
	
	@Query("select doc from Formation doc where doc.proprietaire = :p order by STR_TO_DATE(doc.dateAjout, '%d-%m-%Y') desc ")
	public List<Formation> DocOfProf(@Param("p") Formateur p);	
	
}
