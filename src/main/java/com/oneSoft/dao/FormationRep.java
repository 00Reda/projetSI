package com.oneSoft.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.oneSoft.entities.Formateur;
import com.oneSoft.entities.Formation;

public interface FormationRep extends JpaRepository<Formation, Long>{

	@Transactional
	@Modifying
	@Query(" delete from Formation where id_formation = :id and proprietaire = :user")
	public void delete(@Param("id") long id , @Param("user") Formateur p);
	
	@Query(" select docs from Formation docs where docs.id_formation = :id and docs.proprietaire = :user ")
	public Formation findByIdAndOwner(@Param("id") long id , @Param("user") Formateur p);
	
}
